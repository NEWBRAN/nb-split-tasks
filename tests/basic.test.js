// TDD to build the split task with cpu load caculcation and multiple
// array of promises
const test = require('ava')
const { join } = require('path')
const debug = require('debug')('nb-split-tasks:test')
const nbSplitTasks = require('../index')

test.before(t => {
  t.context.calFn = join(__dirname, 'fixtures', 'cal.js')
  // create an array of paths
  const calPaths = Array(50).fill(t.context.calFn)
  const objPaths = Array(10).fill(join(__dirname, 'fixtures', 'obj.js'))

  let args1 = []
  for (let i = 0; i < 50; ++i) {
    let x = i+1;
    let y = x+1;
    args1.push([x, y])
  }
  let args2 = []
  for (let j = 0; j < 10; ++j) {
    let key = 'a' + j;
    args2.push([key, j])
  }

  t.context.tests = [
    { paths: calPaths, args: args1},
    { paths: objPaths, args: args2}
  ]

  t.context.esModulePath = join(__dirname, 'fixtures', 'es.js')

})

test.cb(`Should able to return an array of results`, t => {
  t.plan(1)
  const { paths, args } = t.context.tests[0]
  nbSplitTasks(paths, args, 'array')
    .then(result => {
      debug(result)
      t.true(Array.isArray(result))
      t.end()
    })
})

test.cb(`It should able to accept the path to function as string`, t => {
  t.plan(1)
  const { args } = t.context.tests[0]
  nbSplitTasks(t.context.calFn, args, 'array')
    .then(result => {
      debug(result)
      t.true(Array.isArray(result))
      t.end()
    })
})

test.cb(`Should able to return the result as one object`, t => {
  t.plan(1)
  const { paths, args } = t.context.tests[1]
  nbSplitTasks(paths, args)
    .then(result => {
      debug(result)
      t.true(typeof result === 'object')
      t.end()
    })
})

test.cb(`It should able to merge into an pre-defined object`, t => {
  t.plan(2)
  const { paths, args } = t.context.tests[1]
  nbSplitTasks(paths, args, {x: 'y'})
    .then(result => {
      debug(result)
      t.true(typeof result === 'object')
      t.is(result.x, 'y')
      t.end()
    })
})

test.cb(`It should able to handle an ES6 function`, t => {
  t.plan(1)
  const { args } = t.context.tests[0]
  nbSplitTasks(t.context.esModulePath, args, 'array')
    .then(result => {
      debug(result)
      t.true(Array.isArray(result))
      t.end()
    })
})
