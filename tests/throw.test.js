const test = require('ava')
const { join } = require('path')
const GeneralError = require('jsonql-errors/general')
const debug = require('debug')('nb-split-tasks:test:error')
const nbSplitTasks = require('../index')

const throwErr = () => {
  throw new Error('Just some random Error')
}

const throwWrapper = () => {
  try {
    throwErr()
  } catch(e) {
    throw new GeneralError(e, {txt: 'me'})
  }
}


test(`Just testing the General Error`, t => {

  const error = t.throws( () => {
    return throwWrapper()
  } , GeneralError, 'Throw a dummy error then wrap it with GeneralError')

  debug(error)

  t.is(error.className, 'GeneralError')
  t.is(error.detail.txt, 'me')

})

test.cb.skip(`Test a function that throw error from within the fork`, t => {
  const pathToFn = join(__dirname, 'fixtures', 'err.js')
  let args = []
  for (let i = 0; i < 20; ++i) {
    args.push(i)
  }
  nbSplitTasks(pathToFn, args)
    .catch(err => {
      debug(err)
      t.pass()
      t.end()
    })

})
