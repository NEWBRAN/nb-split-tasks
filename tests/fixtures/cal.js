
/**
 * Just a very simple add method
 * @param {number} x
 * @param {number} y
 * @return {number} sum of x and y
 */
module.exports = function cal(x, y) {
  return x + y;
}
