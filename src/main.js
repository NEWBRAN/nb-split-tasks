// the main method to call
const os = require('os')
const { fork } = require('child_process')
const { join } = require('path')
const { NOT_ENOUGH_CPU } = require('../constants')
const merge = require('lodash.merge')
const { chainPromises } = require('jsonql-utils')

/**
 * test if we have enough cpu slots
 * @return {int} available cpu slots
 */
function getSlots() {
  // we need to reduce the number of slot because it could hang the computer
  // if we call too many
  let total = os.cpus().length;
  return total - 1; // minu one found that makes no different
  /*
  if (total >= 8) { // quad core, good
    return Math.round(total / 2) // use half
  } else {
    const canUse = total - 1;
  }
  */
}

/**
 * Get the fork option and validate it (@TODO)
 * @param {string} scriptPath to add to the first in array
 * @param {boolean|object} forkOption
 * @return {array} if it passed
 */
function getForkOption(scriptPath, opt) {
  let arg = [scriptPath]
  const additionalArg = opt !== false && typeof opt === 'object' && !Array.isArray(opt) ? opt : []
  return [...arg, additionalArg]
}

/**
 * calling the external process
 * @param {number} id to id this task
 * @param {string} pathToFn the path to the function for processing
 * @param {array} args to pass the argument to the processor
 * @param {object|boolean} forkOption pass to the fork process if any
 * @return {promise} resolve the result if any
 */
function executeTask(id, pathToFn, args, forkOption) {
  const scriptPath = join(__dirname, 'ps.js')
  const forkArgs = getForkOption(scriptPath, forkOption)
  const ps = Reflect.apply(fork, null, forkArgs)
  return new Promise((resolver, rejecter) => {
    // send payload
    ps.send({ id, pathToFn, args })
    // wait for result
    ps.on('message', result => {
      // then kill this process
      ps.kill()
      // process the result
      const { error } = result;
      if (error) {
        return rejecter(result)
      }
      resolver(result)

    })
    // when the child crash we will get an `exit` event
    ps.on('exit', (code, signal) => {
      rejecter({ code, signal })
    })
  })
}

/**
 * get value and check
 * @param {string} returnType what to expect
 * @return {boolean} for our own internal methods
 */
function getReturnType(returnType) {
  switch(returnType) {
    case 'object':
      return true;
    case 'array':
      return false;
    default:
      if (!Array.isArray(returnType) && typeof returnType === 'object') {
        return returnType; // just return the init object
      }
      throw new Error(`Unexpected returnType: ${returnType} only accept 'object' or 'array'`)
  }
}

/**
 * because we want the matrix sub task run in parellel therefore, we are not using chainPromises
 * @param {boolean} returnType true is object false is array
 * @return {function} the actual processor
 */
function processResult(returnType) {
  /**
   * the return result with the following signature
   * id , result
   */
  return function processor(results) {
    const toProcess = results.map(({ result }) => result)
    return returnType === false
      ? toProcess
      : Reflect.apply(merge, null, toProcess)
  }
}

/**
 * make this cleaner one step at a time
 * @param {number} slots total available cal slots
 * @param {number} ctn total of matrix
 * @param {number} taskCtn the all task count
 * @param {array} taskPaths to get the function
 * @param {array} payloads all the arguments
 * @param {boolean} returnType object or array
 * @param {object|boolean} forkOption pass to the fork process if any
 * @return {array} the matrix
 */
function createSubTask(slots, ctn, taskCtn, taskPaths, payloads, returnType, forkOption) {
  let matrix = []
  for (let i=0; i<ctn; ++i) {
    let begin = i * slots;
    let end = begin + slots;
    if (end >= taskCtn) {
      end = taskCtn;
    }
    matrix.push(
      Promise.all(
        payloads.slice(begin, end).map((payload, x) => {
          let arg = Array.isArray(payload) ? payload : [payload]
          return executeTask(x+i, taskPaths[x], arg, forkOption)
        })
      ).then(
        processResult(returnType)
      ).catch(err => {
        console.error(`[nb-split-tasks] One of the task failed`, err)
      })
    )
  }
  return matrix;
}

/**
 * 1. default array just put each result into the array and return it, we don't care the order
 * 2. object then merge every result together into one object, we don't care the order
 * create the task matrix
 * @param {int} slots cpu slots
 * @param {string} taskPath file name of the method
 * @param {array} payloads array
 * @param {boolean} returnType array or object
 * @param {boolean|object} forkOption option to pass to the fork call
 * @return {promise} resolve the final result
 */
function createTask(slots, taskPaths, payloads, returnType, forkOption) {
  let taskCtn = payloads.length;
  let ctn = Math.ceil(taskCtn/slots)
  // now futher process the final matrix
  return chainPromises(
    createSubTask(slots, ctn, taskCtn, taskPaths, payloads, returnType, forkOption),
    returnType
  ).then( // when return as array it becomes array of array but we want to flat it
    result => returnType === false ? result.reduce((x, y) => x.concat(y), []) : result
  )
}

/**
 * the main api to start the split task process
 * @param {string|array} pathToFn to each callback
 * @param {array} args array of arguments
 * @param {string} returnType array or object
 * @param {boolean|object} [forkOption=false] option to pass to the fork call
 * @return {promise} resolve to the final outcome
 */
module.exports = function main(pathToFn, args, returnType = 'object', forkOption = false) {
  const slots = getSlots()
  if (slots === 0) {
    return Promise.reject(NOT_ENOUGH_CPU)
  }
  if (Array.isArray(args)) {
    let pathToFns;
    const ctn = args.length;
    if (!Array.isArray(pathToFn)) {
      pathToFns = Array(ctn).fill(pathToFn)
    } else {
      pathToFns = pathToFn; // just pass it over
      // double check it
      if (ctn !== pathToFn.length) {
        return Promise.reject(`The path to function length is not the same as the arguments!`)
      }
    }
    // finally we can start
    return createTask(slots, pathToFns, args, getReturnType(returnType), forkOption)
  }
  return Promise.reject(`Expect both path to function and arguments are array`)
}
