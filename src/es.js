// import es module related methods
const fs = require('fs')
// we just use the esm to do the require regardless of their type
require = require("esm")(module)
/**
 * check if the file is ES6 module but it could be wrong
 * @param {string} pathToFn path to function
 * @return {boolean} true or false
 */
function isEsModule(pathToFn) {
  return new Promise((resolver, rejecter) => {
    fs.readFile(pathToFn, 'utf8', (err, data) => {
      if (err) {
        return rejecter(err)
      }
      if (data.toString().indexOf('module.exports') > -1) {
        return resolver(false)
      }
      resolver(true)
    })
  })
}

/**
 * @NOTE we don't check the file to reduce overhead
 * main method to require the function into the ps
 * @param {string} pathToFn the path to the function
 * @return {function} the function itself to execute
 */
function requireFn(pathToFn) {
  return isEsModule(pathToFn)
    .then(es => {
      if (es) {
        return requireEsModule(pathToFn)
      }
      return require(pathToFn)
    })
}

/**
 * This is ported from jsonql-resolver
 * Try to use the esm module to require the module directly
 * @param {string} pathToFn path to resolver from contract
 * @return {*} resolver function on success
 */
module.exports = function requireEsModule(pathToFn) {
  return new Promise((resolver, rejecter) => {
    try {
      //oldRequire = require;
      const obj = require(pathToFn)
      if (typeof obj === 'function') {
        return resolver(obj)
      } else if (obj.default && typeof obj.default === 'function') {
        return resolver(obj.default)
      }
      rejecter(new Error(`Unable to import module from ${pathToFn}!`))
    } catch (e) {
      rejecter(new Error(e))
    }
  })

}
