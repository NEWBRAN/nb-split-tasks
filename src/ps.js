// this is the file to run the process
const { resolve } = require('path')
const requireFn = require('./es')
const GeneralError = require('jsonql-errors/general')

process.on('message', function runProcess({ id, pathToFn, args }) {
  requireFn(pathToFn)
    .then(fn => Reflect.apply(fn, null, args))
    .then(result => {
      process.send({id, result})
    })
    .catch(error => {
      console.error(error)
      // when we send it separately the orginal error was lost
      // error.detail = {id, pathToFn, args}
      process.send({ error: new GeneralError(error.message, {id, pathToFn, args}) })
    })
})
